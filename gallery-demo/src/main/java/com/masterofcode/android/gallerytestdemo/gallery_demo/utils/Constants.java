package com.masterofcode.android.gallerytestdemo.gallery_demo.utils;

/**
 * Created by boss1088 on 5/19/14.
 */
public class Constants {
    public static final String PACKAGE = "com.masterofcode.android.gallerytestdemo.gallery_demo";
    public static final float sAnimatorScale = 1;
    public static final int ANIM_DURATION = 500;
}
