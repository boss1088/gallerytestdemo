package com.masterofcode.android.gallerytestdemo.gallery_demo.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import android.os.Handler;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.masterofcode.android.gallerytestdemo.gallery_demo.R;
import com.masterofcode.android.gallerytestdemo.gallery_demo.fragments.PictureViewFragment;
import com.masterofcode.android.gallerytestdemo.gallery_demo.objects.PictureInfoParcelable;
import com.masterofcode.android.gallerytestdemo.gallery_demo.utils.Constants;

public class PictureViewPagerActivity extends ActionBarActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    private ArrayList<PictureInfoParcelable> items = new ArrayList<>();
    private int currentItem;
    private int orientation;

    private Bundle arguments;
    private boolean doNotShowAnim = false;

    private boolean isBack = false;
    private boolean isVisibleActionBar;

    private ActionBar actionBar;

    Runnable hideActionBar = new Runnable() {
        @Override
        public void run() {
            actionBar.hide();
            isVisibleActionBar = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_view_pager);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_background));

        List<Fragment> fragments = new Vector<>();
        if (savedInstanceState == null) {
            arguments = getIntent().getExtras();

            currentItem = arguments.getInt(Constants.PACKAGE + ".currentItem");
            orientation = arguments.getInt(Constants.PACKAGE + ".orientation");
            items = arguments.getParcelableArrayList(Constants.PACKAGE + ".pictureObjects");


            for (int i = 0; i < items.size(); i++) {
                Bundle args = new Bundle();
                args.putInt(Constants.PACKAGE + ".orientation", orientation);
                args.putParcelable(Constants.PACKAGE + ".parcelItem", items.get(i));
                args.putBoolean(Constants.PACKAGE + ".isCurrent", (i == currentItem && !doNotShowAnim));
                fragments.add(PictureViewFragment.newInstance(i, args));
            }
        } else {
            arguments = savedInstanceState.getBundle(Constants.PACKAGE + ".bundle");
            currentItem = savedInstanceState.getInt(Constants.PACKAGE + ".currentItem");
            doNotShowAnim = true;
            int count = savedInstanceState.getInt(Constants.PACKAGE + ".countOfAdapter");
            for (int i = 0; i < count; i++) {
                fragments.add(getSupportFragmentManager().getFragment(savedInstanceState, Constants.PACKAGE + String.valueOf(i)));
            }
        }

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(mSectionsPagerAdapter.getCount());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(currentItem);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentItem = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        new Handler().postDelayed(hideActionBar, 2000);
    }

    public void showHideActionBar() {
        if (isVisibleActionBar) {
            actionBar.hide();
            isVisibleActionBar = false;
        } else {
            actionBar.show();
            isVisibleActionBar = true;
            new Handler().postDelayed(hideActionBar, 2000);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Constants.PACKAGE + ".currentItem", currentItem);
        outState.putBundle(Constants.PACKAGE + ".bundle", arguments);
        outState.putInt(Constants.PACKAGE + ".countOfAdapter", mSectionsPagerAdapter.getCount());
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            getSupportFragmentManager().putFragment(outState, Constants.PACKAGE + String.valueOf(i),
                                                        mSectionsPagerAdapter.getItem(i));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.picture_view_pager, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            int current = mViewPager.getCurrentItem();
            PictureViewFragment fragment = (PictureViewFragment) mSectionsPagerAdapter.getItem(current);
            fragment.saveImageToGallery();
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private List<Fragment> fragments;

        FragmentManager fm;

        public SectionsPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fm = fm;
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                default:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!isBack) {
            isBack = true;

            int current = mViewPager.getCurrentItem();
            PictureViewFragment fragment = (PictureViewFragment) mSectionsPagerAdapter.getItem(current);

            fragment.runExitAnimation(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        }
    }

    @Override
    public void finish() {
        super.finish();

        // override transitions to skip the standard window animations
        overridePendingTransition(0, 0);
    }

}
