package com.masterofcode.android.gallerytestdemo.gallery_demo.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by boss1088 on 5/20/14.
 */
public class PictureInfoParcelable implements Parcelable {

    int resourceId;
    int left;
    int top;
    int width;
    int height;

    public PictureInfoParcelable(int resourceId, int left, int top, int width, int height) {
        this.resourceId = resourceId;
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public PictureInfoParcelable(Parcel in) {
        int[] data = new int[5];
        in.readIntArray(data);
        setResourceId(data[0]);
        setLeft(data[1]);
        setTop(data[2]);
        setWidth(data[3]);
        setHeight(data[4]);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeIntArray(new int[] { getResourceId(),
                                      getLeft(),
                                      getTop(),
                                      getWidth(),
                                      getHeight()});
    }

    public static final Creator CREATOR = new Creator() {

        @Override
        public PictureInfoParcelable createFromParcel(Parcel in) {
            return new PictureInfoParcelable(in);
        }

        @Override
        public PictureInfoParcelable[] newArray(int size) {
            return new PictureInfoParcelable[size];
        }
    };
}
