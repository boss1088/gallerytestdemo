package com.masterofcode.android.gallerytestdemo.gallery_demo.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.masterofcode.android.gallerytestdemo.gallery_demo.R;
import com.masterofcode.android.gallerytestdemo.gallery_demo.activities.PictureViewPagerActivity;
import com.masterofcode.android.gallerytestdemo.gallery_demo.objects.PictureInfoParcelable;
import com.masterofcode.android.gallerytestdemo.gallery_demo.utils.BitmapUtils;
import com.masterofcode.android.gallerytestdemo.gallery_demo.utils.Constants;
import com.masterofcode.android.gallerytestdemo.gallery_demo.utils.Intents;
import com.masterofcode.android.gallerytestdemo.gallery_demo.views.TouchImageView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;

import java.util.Date;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

/**
 * Created by boss1088 on 5/19/14.
 */
public class PictureViewFragment extends Fragment {

    private static final Interpolator sDecelerator = new DecelerateInterpolator();
    private static final Interpolator sAccelerator = new AccelerateInterpolator();

    private BitmapDrawable mBitmapDrawable;
    private ColorMatrix colorizerMatrix = new ColorMatrix();
    ColorDrawable mBackground;
    int mLeftDelta;
    int mTopDelta;
    float mWidthScale;
    float mHeightScale;
    private ImageView mImageView;
    private FrameLayout mTopLevelLayout;
    private int mOriginalOrientation;

    private Bundle mArguments;
    boolean isCurrent;

    public static PictureViewFragment newInstance(int sectionNumber, Bundle args) {
        PictureViewFragment fragment = new PictureViewFragment();
        if (args == null) {
            args = new Bundle();
        }

        args.putInt(Intents.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        fragment.mArguments = args;
        return fragment;
    }

    public PictureViewFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mArguments = getArguments();
        isCurrent = mArguments.getBoolean(Constants.PACKAGE + ".isCurrent");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_picture_view, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mImageView = (ImageView) getView().findViewById(R.id.imageView);
        mTopLevelLayout = (FrameLayout) getView().findViewById(R.id.topLevelLayout);

        PictureInfoParcelable item = mArguments.getParcelable(Constants.PACKAGE + ".parcelItem");

        Bitmap bitmap = BitmapUtils.getBitmap(getResources(), item.getResourceId());
        final int thumbnailTop = item.getTop();
        final int thumbnailLeft = item.getLeft();
        final int thumbnailWidth = item.getWidth();
        final int thumbnailHeight = item.getHeight();
        mOriginalOrientation = mArguments.getInt(Constants.PACKAGE + ".orientation");

        mBitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        mImageView.setImageDrawable(mBitmapDrawable);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PictureViewPagerActivity) getActivity()).showHideActionBar();
            }
        });

        mBackground = new ColorDrawable(Color.BLACK);
        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mTopLevelLayout.setBackgroundDrawable(mBackground);
        } else {
            mTopLevelLayout.setBackground(mBackground);
        }

        if (isCurrent) {
            mArguments.putBoolean(Constants.PACKAGE + ".isCurrent", false);
            ViewTreeObserver observer = mImageView.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    mImageView.getViewTreeObserver().removeOnPreDrawListener(this);

                    // Figure out where the thumbnail and full size versions are, relative
                    // to the screen and each other
                    int[] screenLocation = new int[2];
                    mImageView.getLocationOnScreen(screenLocation);

                    Matrix matrix = mImageView.getImageMatrix();
                    float[] values = new float[12];
                    matrix.getValues(values);
                    float startY = values[5];
                    float startX = values[2];

                    // Scale factors to make the large version the same size as the thumbnail
                    mWidthScale = (float) thumbnailWidth / (mImageView.getDrawable().getIntrinsicWidth() * values[0]);
                    mHeightScale = (float) thumbnailHeight / (mImageView.getDrawable().getIntrinsicHeight() * values[4]);

                    mLeftDelta = (int) (thumbnailLeft - startX * mHeightScale - screenLocation[0]);
                    mTopDelta = (int) (thumbnailTop - startY * mHeightScale - screenLocation[1]);

                    runEnterAnimation();

                    return true;
                }
            });
        }
    }

    public void runEnterAnimation() {
        final long duration = (long) (Constants.ANIM_DURATION * Constants.sAnimatorScale);

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        ViewHelper.setPivotX(mImageView, 0);
        ViewHelper.setPivotY(mImageView, 0);
        ViewHelper.setScaleX(mImageView, mWidthScale);
        ViewHelper.setScaleY(mImageView, mHeightScale);
        ViewHelper.setTranslationX(mImageView, mLeftDelta);
        ViewHelper.setTranslationY(mImageView, mTopDelta);

        animate(mImageView).setDuration(duration).
                scaleX(1).scaleY(1).
                translationX(0).translationY(0)
        .setInterpolator(sDecelerator);

        // Animate scale and translation to go from thumbnail to full size
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0, 255);
        bgAnim.setDuration(duration);
        bgAnim.start();

        // Animate a drop-shadow of the image
        ObjectAnimator shadowAnim = ObjectAnimator.ofFloat(mTopLevelLayout, "shadowDepth", 0, 1);
        shadowAnim.setDuration(duration);
        shadowAnim.start();
    }

    public void runExitAnimation(final Runnable endAction) {
        final long duration = (long) (Constants.ANIM_DURATION * Constants.sAnimatorScale);

        PictureInfoParcelable item = mArguments.getParcelable(Constants.PACKAGE + ".parcelItem");

        final int thumbnailTop = item.getTop();
        final int thumbnailLeft = item.getLeft();
        final int thumbnailWidth = item.getWidth();
        final int thumbnailHeight = item.getHeight();

        if (((TouchImageView) mImageView).isZoomed()) {
            ((TouchImageView) mImageView).resetZoom();
        }

        Matrix matrix = mImageView.getImageMatrix();
        float[] values = new float[12];
        matrix.getValues(values);
        float startY = values[5];
        float startX = values[2];

        // Scale factors to make the large version the same size as the thumbnail
        mWidthScale = (float) thumbnailWidth / (mImageView.getDrawable().getIntrinsicWidth() * values[0]);
        mHeightScale = (float) thumbnailHeight / (mImageView.getDrawable().getIntrinsicHeight() * values[4]);

        // Figure out where the thumbnail and full size versions are, relative
        // to the screen and each other
        int[] screenLocation = new int[2];
        mImageView.getLocationOnScreen(screenLocation);

        mLeftDelta = (int) (thumbnailLeft - startX * mHeightScale - screenLocation[0]);
        mTopDelta = (int) (thumbnailTop - startY * mHeightScale - screenLocation[1]);

        // Caveat: configuration change invalidates thumbnail positions; just animate
        // the scale around the center. Also, fade it out since it won't match up with
        // whatever's actually in the center
        final boolean fadeOut;
        if (getResources().getConfiguration().orientation != mOriginalOrientation) {
            ViewHelper.setPivotX(mImageView, (mImageView.getDrawable().getIntrinsicWidth() * values[0]) / 2);
            ViewHelper.setPivotY(mImageView, (mImageView.getDrawable().getIntrinsicHeight() * values[4]) / 2);
            mLeftDelta = 0;
            mTopDelta = 0;
            fadeOut = true;
        } else {
            ViewHelper.setPivotX(mImageView, mWidthScale);
            ViewHelper.setPivotY(mImageView, mHeightScale);
            ViewHelper.setScaleX(mImageView, 1);
            ViewHelper.setScaleY(mImageView, 1);
            ViewHelper.setTranslationX(mImageView, 0);
            ViewHelper.setTranslationY(mImageView, 0);
            fadeOut = false;
        }

        animate(mImageView).setDuration(duration).setInterpolator(sAccelerator).
                scaleX(mWidthScale).scaleY(mHeightScale).
                translationX(mLeftDelta).translationY(mTopDelta).
                setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        getActivity().finish();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) { }

                    @Override
                    public void onAnimationRepeat(Animator animator) {}
                });
        if (fadeOut) {
            animate(mImageView).alpha(0);
        }
        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(mBackground, "alpha", 0);
        bgAnim.setDuration(duration);
        bgAnim.start();

        // Animate the shadow of the image
        ObjectAnimator shadowAnim = ObjectAnimator.ofFloat(mTopLevelLayout,
                "shadowDepth", 1, 0);
        shadowAnim.setDuration(duration);
        shadowAnim.start();
    }

    public void saveImageToGallery() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                PictureInfoParcelable item = mArguments.getParcelable(Constants.PACKAGE + ".parcelItem");
                String date = new Date().toString();

                if (getActivity() != null) {
                    MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),
                            mBitmapDrawable.getBitmap(), date + "-" + String.valueOf(item.getResourceId()), date);
                }

                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(getActivity(), "Image saved to Gallery to Camera folder", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });
                }
            }
        }).start();

    }
}
